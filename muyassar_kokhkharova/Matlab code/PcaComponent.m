function PcaComponent(csiDenoised, sampleStartIndex, sampleEndIndex, numOfSamples, statisticalData)
       global statisticalData
        global numOfSamples
    [coeff, pcaData,latend, tsd,varience]=pca(csiDenoised);
    % figure
    % plot(pcaData);

    %take 2,3 components
    pcaComponents=zeros([sampleEndIndex-sampleStartIndex+1,2]);
    for i=1: 1: 3
    pcaComponents(:,i)=pcaData(:,i);
    end
    %  figure
    %  plot(pcaComponents);
    FeatureExtraction(pcaComponents, numOfSamples, statisticalData);
end