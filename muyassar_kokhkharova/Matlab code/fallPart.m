function fallPart(filename)
    
    global fallCsiMatrix;
    
    csi_trace1 = read_bf_file(filename);
    fileSize = size(csi_trace1);
    fileSize = fileSize(1,1);
    timeStamps=zeros(1,fileSize);
    cell=cell2mat(csi_trace1(:,1));
    str=struct2cell(cell);
    strucTS=str(1,:);
    fallCsiMatrix = zeros([fileSize, 3*3*30]);

    for i = 1 : 1 : fileSize
        timeStamp = strucTS(1,i);
        timeStamps(1,i) = cell2mat(timeStamp);
    end

    for i = 1 : 1 : fileSize
        %csiData=cell.csi;
        row=str(12,i);
        row=cell2mat(row);
        ampl= abs(row);
        %transpAmpl=transpose(ampl);
        reshaped=reshape(ampl, 3, 30*3);  
        reshaped=reshape(transpose(reshaped),1,[]);
        fallCsiMatrix(i,:)=reshape(reshaped, 1, 3*3*30);
    end
    
    Split(fallCsiMatrix, fileSize, timeStamps);
end

% Split by TimeStamp
function Split(fallCsiMatrix, fileSize, timeStamps)
    global fallCsiMatrix
    global numOfSamples
    global afterFallData
    sampleEndIndex=1;
    fall = 1;
    lie = 0;
    for i = 1 : 1: fileSize-1
        if (timeStamps(1,i+1)-timeStamps(1,i) > 200000 || i==fileSize-1)
            if fall ==1
                numOfSamples=numOfSamples+1;
                fall = 0;
                lie = 1;
                sampleStartIndex = sampleEndIndex;
                sampleEndIndex = i;
                OneSampleMatrix(sampleStartIndex,sampleEndIndex);
                %figure
                %Denoise(oneSampleCsiMatrix, sampleStartIndex, sampleEndIndex)
            
            else 
                fall = 1;
                lie = 0;
                sampleStartIndex = sampleEndIndex;
                sampleEndIndex = i;
                OneSample(sampleStartIndex,sampleEndIndex);
            end
        end
    end
end
function OneSampleMatrix(sampleStartIndex,sampleEndIndex)
global fallCsiMatrix
global numOfSamples
global statisticalData

k=0;
        oneSampleCsiMatrix=zeros([sampleEndIndex-sampleStartIndex+1, 270]);
        for j = sampleStartIndex : 1 : sampleEndIndex
            k=k+1;
            oneSampleCsiMatrix(k,:) = fallCsiMatrix(j,:);
        end
        
        Denoise(oneSampleCsiMatrix,sampleStartIndex,sampleEndIndex,numOfSamples, statisticalData)
 end
 
function OneSample(sampleStartIndex,sampleEndIndex)
global fallCsiMatrix
global numOfSamples
global afterFallData
    k=0;
    oneSampleCsi=zeros([sampleEndIndex-sampleStartIndex+1, 270]);
    for j = sampleStartIndex : 1 : sampleEndIndex
        k=k+1;
        oneSampleCsi(k,:) = fallCsiMatrix(j,:);
    end
    DenoiseFallData(oneSampleCsi,sampleStartIndex,sampleEndIndex,numOfSamples, afterFallData)
end

function DenoiseFallData(oneSampleCsi,sampleStartIndex,sampleEndIndex,numOfSamples, afterFallData)

    global afterFallData
    global numOfSamples
    Nr = 3;
    Nt = 3;
    sizeSub = 30;
    oneSampleDenoised = zeros([sampleEndIndex - sampleStartIndex + 1, Nr*Nt*sizeSub]);

    wname='sym6';
    level=4;
    for i = 1 : 1 : Nt*Nr*sizeSub
        %[c, l] = wavedec(csiMatrix(:,i),level,wname);
        fd = wden(oneSampleCsi(:,i), 'heursure','s', 'sln', level, wname);
        %fd = wdenoise(oneSampleCsiMatrix(:,i),4,'Wavelet','sym6','DenoisingMethod','SURE');
 
        oneSampleDenoised(:,i)=fd;
    end
%     figure 
%     plot(oneSampleCsiMatrix);
%     plotting raw and denoised csi data

    dbCsi=mag2db(oneSampleCsi);
    dbDenoised=mag2db(oneSampleDenoised);
      figure();
%       subplot(2,1,1);
%       plot(dbCsi); axis tight; grid on;
%       title('Raw data');
%       subplot(2,1,2);
       plot(dbDenoised);axis tight; grid on;
      title('Standing up after fall');
    PcaComponentFall(oneSampleDenoised, sampleStartIndex, sampleEndIndex, numOfSamples, afterFallData);
end
function PcaComponentFall(csiDenoised, sampleStartIndex, sampleEndIndex, numOfSamples, afterFallData)
       global afterFallData
        global numOfSamples
    [coeff, pcaData,latend, tsd,varience]=pca(csiDenoised);
    % figure
    % plot(pcaData);

    %take 2,3 components
    pcaComponents=zeros([sampleEndIndex-sampleStartIndex+1,3]);
    for i=1: 1: 3
    pcaComponents(:,i)=pcaData(:,i);
    end
%      figure
%      plot(pcaComponents);
    FallFeatureExtraction(pcaComponents, numOfSamples, afterFallData);
end

function FallFeatureExtraction(pcaComponents, numOfSamples, afterFallData)
       global afterFallData
        global numOfSamples
    St = std(pcaComponents);
    MAD = mad(pcaComponents);
    %meanValue = mean(pcaComponents);
    medianValue = median(pcaComponents);
    kurtosisValue = kurtosis(pcaComponents);
    momentSecond = moment(pcaComponents,2);
    momentThird = moment(pcaComponents,3);
    skewnessValue = skewness(pcaComponents);
    afterFallData(numOfSamples,1:21)= [St(1,1),St(1,2),St(1,3), MAD(1,1),MAD(1,2),MAD(1,3), medianValue(1,1),medianValue(1,2),medianValue(1,3),kurtosisValue(1,1),kurtosisValue(1,2),kurtosisValue(1,3),momentSecond(1,1),momentSecond(1,2),momentSecond(1,3),momentThird(1,1),momentThird(1,2),momentThird(1,3),skewnessValue(1,1),skewnessValue(1,2),skewnessValue(1,3)];
end