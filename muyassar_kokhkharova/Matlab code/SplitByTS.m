function SplitByTS(csiMatrix, fileSize, timeStamps, statisticalData, numOfSamples)
       global statisticalData
        global numOfSamples
    sampleEndIndex=1;
    for i = 1 : 1: fileSize-1
        if timeStamps(1,i+1)-timeStamps(1,i) > 200000
            sampleStartIndex = sampleEndIndex;
            sampleEndIndex = i;
            OneSampleMatrix(sampleStartIndex,sampleEndIndex);
            %figure
            %Denoise(oneSampleCsiMatrix, sampleStartIndex, sampleEndIndex)
        end
    end
    
    function OneSampleMatrix(sampleStartIndex,sampleEndIndex)
        k=0;
        oneSampleCsiMatrix=zeros([sampleEndIndex-sampleStartIndex+1, 270]);
        for j = sampleStartIndex : 1 : sampleEndIndex
            k=k+1;
            oneSampleCsiMatrix(k,:) = csiMatrix(j,:);
        end
        numOfSamples=numOfSamples+1;
        Denoise(oneSampleCsiMatrix,sampleStartIndex,sampleEndIndex,numOfSamples, statisticalData)
    end
end