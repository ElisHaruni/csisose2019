function Denoise(oneSampleCsiMatrix, sampleStartIndex, sampleEndIndex, numOfSamples, statisticalData)
       global statisticalData
        global numOfSamples
    Nr = 3;
    Nt = 3;
    sizeSub = 30;
    oneSampleCsiDenoised = zeros([sampleEndIndex - sampleStartIndex + 1, Nr*Nt*sizeSub]);

    wname='sym6';
    level=5;
    for i = 1 : 1 : Nt*Nr*sizeSub
        %[c, l] = wavedec(csiMatrix(:,i),level,wname);
        fd = wden(oneSampleCsiMatrix(:,i), 'heursure','s', 'sln', level, wname);
        %fd = wdenoise(oneSampleCsiMatrix(:,i),4,'Wavelet','sym6','DenoisingMethod','SURE');
 
        oneSampleCsiDenoised(:,i)=fd;
    end
%     figure 
%     plot(oneSampleCsiMatrix);
%     plotting raw and denoised csi data

    dbCsi=mag2db(oneSampleCsiMatrix);
    dbDenoised=mag2db(oneSampleCsiDenoised);
       figure();
%        subplot(2,1,1);
%        plot(dbCsi); axis tight; grid on;
%        title('Raw data');
%        subplot(2,1,2);
%        
%        figure();
        plot(dbDenoised);axis tight; grid on;
        title('Empty');
    PcaComponent(oneSampleCsiDenoised, sampleStartIndex, sampleEndIndex, numOfSamples, statisticalData);
end