
function ReadFileToMatrix(fileName)
    global statisticalData
    global numOfSamples
    csi_trace1 = read_bf_file(fileName);
    fileSize = size(csi_trace1);
    fileSize = fileSize(1,1);
    if fileSize > 16000
    fileSize = 60000;
    end
    timeStamps=zeros(1,fileSize);
    cell=cell2mat(csi_trace1(:,1));
    %matrix=cell.csi;
    str=struct2cell(cell);
    strucTS=str(1,:);
    csiMatrix = zeros([fileSize, 3*3*30]);

    for i = 1 : 1 : fileSize
        timeStamp = strucTS(1,i);
        timeStamps(1,i) = cell2mat(timeStamp);
    end

    for i = 1 : 1 : fileSize
         %csiData=cell.csi;
        row=str(12,i);
        row=cell2mat(row);
        ampl= abs(row);
        %transpAmpl=transpose(ampl);
        reshaped=reshape(ampl, 3, 30*3);  
        reshaped=reshape(transpose(reshaped),1,[]);
        csiMatrix(i,:)=reshape(reshaped, 1, 3*3*30);
    end
    %plot(csiMatrix)
    global statisticalData

    SplitByTS(csiMatrix, fileSize, timeStamps, statisticalData, numOfSamples);
end 

