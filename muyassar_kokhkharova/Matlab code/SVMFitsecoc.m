global statisticalData
global afterFallData
numOfTestSamples = 20;
numOfAct = 5;
numOfTrainData=100;
Y=1:numOfAct*numOfTrainData;
YFall=1:numOfAct*numOfTrainData;
trainData=zeros(numOfTrainData*numOfAct,21);
trainFallData=zeros(numOfTrainData*numOfAct,21);
testData=zeros(numOfAct*numOfTestSamples,21);
testFallData=zeros(numOfAct*numOfTestSamples,21);

YTest=1:numOfTestSamples*numOfAct;

%create matrix with the same dimention and for after fall data
nRow = size(statisticalData,1);
nCol = size(statisticalData, 2);
%afterFallData = zeros(nRow, nCol);

%TODO add after fall data


%create label vector
for i=1:100
    Y(i)=1;
    YFall(i)=0;
end 
for i=101:200
    Y(i)=2;
    YFall(i)=0;
end 
for i=201:300
    Y(i)=3;
    YFall(i)=0;
end 
for i=301:400
    Y(i)=4;
    YFall(i)=0;
end 
for i=401:500
    Y(i)=5;
end
for i=401:450
    YFall(i)=6;
end
for i=451:500
    YFall(i)=5;
end

% for i=501:510
%     YFall(i)=0;
% end

%adding Test data
for i=101:120
    YTest(i-100)=1;
end 
for i=221:240
    YTest(i-200)=2;
end 
for i=341:360
    YTest(i-300)=3;
end 
for i=461:480
    YTest(i-400)=4;
end
for i=581:600
    YTest(i-500)=5;
end 
for i=601:620
    YTest(i-500)=6;
end 


for i=1:100
    trainData(i,:)=statisticalData(i,:);
    trainFallData(i,:)=afterFallData(i,:);
end 
for i=121:220
    trainData(i-20,:)=statisticalData(i,:);
    trainFallData(i-20,:)=afterFallData(i,:);
end 
for i = 241 : 340
     trainData(i-40,:)=statisticalData(i,:);
     trainFallData(i-40,:)=afterFallData(i,:);
end 
for i = 361 : 460
    trainData(i-60,:)=statisticalData(i,:);
    trainFallData(i-60,:)=afterFallData(i,:);
end 
for i = 481 : 580
    trainData(i-80,:)=statisticalData(i,:);
    trainFallData(i-80,:)=afterFallData(i+10,:);
end

for i = 101 : 120
    testData(i-100,:)=statisticalData(i,:);
    testFallData(i-100,:)=afterFallData(i,:);
end 
for i = 221 : 240
    testData(i-200,:)=statisticalData(i,:);
    testFallData(i-200,:)=afterFallData(i,:);
end 
for i = 341 : 360
     testData(i-300,:)=statisticalData(i,:);
     testFallData(i-300,:)=afterFallData(i,:);
end 
for i = 461 : 480
    testData(i-400,:)=statisticalData(i,:);
    testFallData(i-400,:)=afterFallData(i,:);
end
for i = 581 : 600
    testData(i-500,:)=statisticalData(i,:);
    testFallData(i-500,:)=afterFallData(i,:);
end 

for i = 601:620
    testData(i-500,:) = statisticalData(i,:);
    testFallData(i-500,:) = afterFallData(i,:);
end
t = templateSVM('Standardize',true,'KernelFunction','linear');
t
trainDataTrans=transpose(trainData);
MyModelNoFall=fitcecoc(trainDataTrans(:,1:400),Y(1:400),'Learners',t,'ObservationsIn','columns');
MyModel=fitcecoc(trainDataTrans,Y,'Learners',t,'ObservationsIn','columns');
labels = predict(MyModel,testData);
labelsNoFall=predict(MyModelNoFall, testData(1:80,:));
MyModelForFall=fitcecoc(trainFallData(401:500,:),YFall(401:500),'Learners',t);
%labelsForFall = predict(MyModelForFall,testFallData);

C = confusionmat(labelsNoFall, YTest(1:80))/numOfTestSamples;
for i = 1 : numOfTestSamples*numOfAct+20
    if labels(i) == 5
     %%TODO second check for fall
        labels(i)=predict(MyModelForFall,testFallData(i,:));        
    end
end

c2 = confusionmat(labels, YTest)/numOfTestSamples;
