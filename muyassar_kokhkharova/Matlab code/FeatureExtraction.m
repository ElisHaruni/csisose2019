function FeatureExtraction(pcaComponents, numOfSamples, statisticalData)
       global statisticalData
        global numOfSamples
    St = std(pcaComponents);
    MAD = mad(pcaComponents);
    %meanValue = mean(pcaComponents);
    medianValue = median(pcaComponents);
    kurtosisValue = kurtosis(pcaComponents);
    momentSecond = moment(pcaComponents,2);
    momentThird = moment(pcaComponents,3);
    skewnessValue = skewness(pcaComponents);
    statisticalData(numOfSamples,1:21)= [St(1,1),St(1,2),St(1,3), MAD(1,1),MAD(1,2),MAD(1,3), medianValue(1,1),medianValue(1,2),medianValue(1,3),kurtosisValue(1,1),kurtosisValue(1,2),kurtosisValue(1,3),momentSecond(1,1),momentSecond(1,2),momentSecond(1,3),momentThird(1,1),momentThird(1,2),momentThird(1,3),skewnessValue(1,1),skewnessValue(1,2),skewnessValue(1,3)];
end