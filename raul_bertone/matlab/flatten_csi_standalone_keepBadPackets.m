%% =====================================================================================
%% Parses data from an experiment trial (a cell array) and outputs a 3D matrix:
%%
%%   X -> order of arrival of the network packet. Size: varies
%%   Y -> subcarrier index. Subcarriers from the same antenna pair are grouped together. Size: 1026
%%   Z -> power and phase. Size: 2
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

function [I, P] = flatten_csi_standalone_keepBadPackets(file)

    log = read_log_file(file);
    % Subcarriers from the same antenna pair are grouped together
    I = zeros(length(log), 9*114);
    P = zeros(length(log), 9*114);
    
    for i = 1:length(log)
        packet = log(i);
        csi = packet{1}.csi;
  
        for sub = 1:size(csi,3)
           for s = 1:size(csi,2)
              for r = 1:size(csi,1)
                  I(i, sub+114*(s-1 + (r-1)*2)) = abs(csi(r,s,sub)); % real(csi(r,s,sub)); %  save the intensity value
                  P(i, sub+114*(s-1 + (r-1)*2)) = angle(csi(r,s,sub)); % imag(csi(r,s,sub)); % save the phase value
              end
          end
       end
    end
    
end