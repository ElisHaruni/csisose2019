%% =====================================================================================
%% Pre-processes the data obtained by the Atheros CSI Tool and makes it ready to be used
%% for training an LSTM.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de
%% =====================================================================================

minPackets = 80; 

% data files
inPath = 'G:\CSI\data\raw\50ms\'; % the directory where raw data is located
outPath = 'G:\CSI\data\preProcessed\50ms\true_I_denoised\'; % the directory where raw data is located

% select all files that end in 'dat' from the data directory
s = strcat(inPath, '*.dat');
files = dir(s);

% TODO also check subdirectories
for i = 1:length(files)
    fName = files(i).name; % the name of the data file
    filePath = strcat(inPath, fName); % absolute path to the file
    dat = read_log_file(filePath); % parse data
    
    % sanitize and flatten
    [I, P] = flatten_csi_third(dat);
    
    if size(I, 1) < minPackets % if we get fewer than minPackets good packets in a trial, we discart it
        continue;
    end
    
    % denoise
    [DI, DP] = WaveDenoise(I, P, 'sym4', 4);
    
    % build the final matrix
    csi = transpose(DI); % cat(1, transpose(I), transpose(P)); % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
    
    % save to file
    [~, f] = fileparts(fName);
    outFile = strcat(outPath, f, '.mat'); % saves in proprietary matlab '.mat' format
    save(outFile, 'csi')
end