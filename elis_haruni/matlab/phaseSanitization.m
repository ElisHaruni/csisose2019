

%% =====================================================================================
%% function used to check the visual representation of Phase sanitization
%%  there might be issues with the paths since we use static paths.
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


function phaseSanitization(filename)


[filepath,name,ext] = fileparts(filename);
loged_data = read_bf_file(filename);

P=zeros(600,270);
DUWP=zeros(600,270);
index=1;
for i=1:500
     
     cell= loged_data{i};
     csi   = get_scaled_csi(cell); 
     csi_2 = reshape(csi,[],30);
     csi_1 = reshape(csi_2,1,[]);
     
     phase = angle(csi_1); 
    
     %/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data/walking_235_aroundE2.dat
     UWP = unwrap(phase);
     
     P(index,:)= phase;
      DUWP(index,:)= UWP;
       index=index+1;
       
  
end

           % DUWP = wdenoise(P,6,'Wavelet','sym6');         
            figure
            subplot(2,1,1);
            surf (P)
            title('Raw Phase');

            subplot(2,1,2);
            surf(DUWP);
            title('Unfolded phase');
end

