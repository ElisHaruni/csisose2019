%% =====================================================================================
%%Extract CSI Intestiy and phase from binary log files from Intel .....
%% This script prepares the dataset only for extracting intensity from CSI
%%
%% Author: Elis Haruni
%% Email :  haruni@stud.fra-uas.de
%% =====================================================================================

function readCSI_Intel(filename,denoise)

[filepath,name,ext] = fileparts(filename);

 outPath = "/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing/";
 I=zeros(600,270);
 P=zeros(600,270);
 
 loged_data = read_bf_file(filename);
 
 index=1; %this is the inxed used to save each trial
 trial_index=1;
  
 for i=1:length(loged_data)
      
     
     cell= loged_data{i};
     
     RX = cell.Nrx;
     TX = cell.Ntx;
     
     csi   = get_scaled_csi(cell); 
     csi_2 = reshape(csi,[],30);
     csi_1 = reshape(csi_2,1,[]);
     
     
     I(index,:) = abs(csi_1);
     P(index,:) = angle(csi_1);
     
     index=index+1;
    
     current_ts = cell.timestamp_low;
     if(i<length(loged_data))
         next_ts = loged_data{i+1}.timestamp_low;
         diff_ts = next_ts - current_ts;
     end
       
     if(diff_ts > 200000)
         
         %%denoise and save matrixes as mat files and reset I and P
         
         %firstly cut the 0 rows that was not fullfilled
         I=I(any(I,2),:);
         P=P(any(P,2),:);
         
         index=1;
               
         [DI,DP]= WaveDenoise(I,P,'sym6',6);
         
         if(denoise)
             I= single(I);
             P= single(P);
             final_trial = cat(1, transpose(I)); % we transpose the matrices because the sequenceInputLayer in the LSTM likes them with the time as Y axis
             outFile=strcat(outPath,'raw/',name,int2str(trial_index),'.mat');
             save(outFile,'final_trial');
         end
   
         
         
            DI = single(DI);
            DP = single(DP);
            
            %the following block was used to check the graphs
           % figure
           % subplot(2,1,1);
           % plot(P);
           % title('Raw data')

            %subplot(2,1,2);
            %plot(DP);
            %title('After Wavelet')
         
            
            % build the final matrix
         
          % we transpose the matrices because the sequenceInputLayer in the 
          %LSTM likes them with the time as Y axis
        
          final_trial = cat(1, transpose(DI));
          outFile=strcat(outPath,name,int2str(trial_index),'.mat');
          save(outFile,'final_trial');         
          trial_index = trial_index+1;
         
         
         
          I=zeros(600,270);
          P=zeros(600,270);
        
         
     end
     
         
     
 end




        


end

