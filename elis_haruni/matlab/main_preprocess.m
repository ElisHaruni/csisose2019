
%% =====================================================================================
%% Peprocessing script to parse the datase for activity recognitnion model
%%  there might be issues with the paths since we use static paths.
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


inputPath="/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data/";

s=strcat(inputPath,'*.dat');
files=dir(s);
%filename= "/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data/walking_235_around.dat";

datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF')

for i=1:length(files)
    fname=files(i).name;
    filepath = strcat(inputPath,fname); 
    %readCSI_Intel(filepath,false); %%read only intensity
    read_csi_intel_Phase_Intestiy(filepath); %%read with phase and intesity 
end

datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF')