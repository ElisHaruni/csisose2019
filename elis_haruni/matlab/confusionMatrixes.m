
%% =====================================================================================
%% This script loads the trained netowrks and some test data to build
%% confusion matrixes for each model 
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


testDataPathCC =  '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_onr_test/'; % '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase_test/'; test data path
testDataPathAC =  '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_test/'; % '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase_test/'; test data path

%loading Test data  to matlab
[testSequencesCC, testLabelsCC] = buildDataSet(testDataPathCC);
[testSequencesAC, testLabelsAC] = buildDataSet(testDataPathAC);

%careful you have to cahnge this based on the minibach size you have used
%on your train
miniBatchSize = 20;
%loading networks at matlab

network = load('trained_crowd_counting_last.mat');
trainNETCC = network.trainedNet;
network = load('train_ intesity_shuffle_every_epoch_2020.mat');
trainNETAC =  network.trainedNet;


predLabelsCC = classify(trainNETCC, testSequencesCC, 'MiniBatchSize', miniBatchSize);
predLabelsAC = classify(trainNETAC, testSequencesAC, 'MiniBatchSize', miniBatchSize);

%ypredAC = predict(trainNETAC,testSequencesAC);
%figure 
%plotconfusion(testLabelsCC,predLabelsCC);

%figure
%plotconfusion(testLabelsAC,predLabelsAC);

%confMatrix= zeros(10,10);
%confMatrix(1,:) = mean(ypredAC(1:56,:));
%confMatrix(2,:) = mean(ypredAC(57:89,:));
%confMatrix(3,:) = mean(ypredAC(90:130,:));
%confMatrix(4,:) = mean(ypredAC(131:167,:));
%confMatrix(5,:) = mean(ypredAC(168:197,:));
%confMatrix(6,:) = mean(ypredAC(198:224,:));
%confMatrix(7,:) = mean(ypredAC(225:251,:));
%confMatrix(8,:) = mean(ypredAC(252:281,:));
%confMatrix(9,:) = mean(ypredAC(282:313,:));
%confMatrix(10,:) = mean(ypredAC(314:360,:));


%pred1 = classify(trainNETAC, testSequencesAC, 'MiniBatchSize', miniBatchSize);

[ConfAC,orderAC]=confusionmat(testLabelsAC,predLabelsAC);
[ConfCC,orderCC]=confusionmat(testLabelsCC,predLabelsCC);



figure
confusionchart(testLabelsAC,predLabelsAC)
figure
confusionchart(testLabelsCC,predLabelsCC)


