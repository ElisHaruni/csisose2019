%% =====================================================================================
%% 1. Creates training and test data-sets from Atheros CSI_Tool files.
%% 2. Trains an LSTM using the training data-set.
%% 3. Calculates the network accuracy using the test data-set.
%%
%%      Author:  Raul Bertone
%%      Email :  bertone@stud.fra-uas.de 
%% =====================================================================================

% << PARAMETERS >>
miniBatchSize = 20; % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set

% data path
networkName = 'trained_crowd_counting_last'; %120 srand for 1 minibachsize and 20 maxepochs
trainDataPath = '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_onr/'; % '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase/';train data path
testDataPath =  '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_onr_test/'; % '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase_test/'; test data path

% TRAINING DATA-SET
% select all files that end in '.mat' from the directory
[trainSequences, trainLabels] = buildDataSet(trainDataPath);

% TEST DATA-SET
% select all files that end in '.mat' from the directory
 [testSequences, testLabels] = buildDataSet(testDataPath);

% train the net for crowd cownting 
trainedNet = crowdCountingLSTM(trainSequences, trainLabels, testSequences, testLabels,miniBatchSize);
%uncoment the following line and coment the previous to train for activity
%detection
%%trainedNet = crowdCountingLSTM(trainSequences, trainLabels, testSequences, testLabels,miniBatchSize);

% save the trained network to file
fileName = strcat(networkName, '.mat');
save(fileName, 'trainedNet')

% classify the test data
predLabels = classify(trainedNet, testSequences, 'MiniBatchSize', miniBatchSize);

% calculate the classification accuracy of the predictions
acc = sum(predLabels == testLabels)./numel(testSequences);

