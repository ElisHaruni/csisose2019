
%% =====================================================================================
%% Peprocessing script to parse the datase for crowd counting  recognitnion network
%%  there might be issues with the paths since we use static paths.
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================

inputPath="/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_cr/";

s=strcat(inputPath,'*.dat');
files=dir(s);
%filename= "/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data/walking_235_around.dat";

datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF')

for i=1:length(files)
    fname=files(i).name;
    filepath = strcat(inputPath,fname); 
    %readCSI_Intel(filepath,false);
    read_only_numbers(filepath); 
end

datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF')