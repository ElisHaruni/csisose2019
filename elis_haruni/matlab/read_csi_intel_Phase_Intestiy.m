
%% =====================================================================================
%% Function that  helps parsing the dataset for activity recognitnion model for Intensity and Phase
%%  There might be issues with the paths since we use static paths.
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================



function read_csi_intel_Phase_Intestiy(filename)

[filepath,name,ext] = fileparts(filename);
 outPath = "/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase/";
 
 I=zeros(600,270);
 P=zeros(600,270);
 loged_data = read_bf_file(filename);
 index=1; %this is the inxed used to save each trial
 trial_index=1;
  
 for i=1:length(loged_data)
      
     
     cell= loged_data{i};
     
     RX = cell.Nrx;
     TX = cell.Ntx;
     
     csi   = get_scaled_csi(cell); 
     csi_2 = reshape(csi,[],30);
     csi_1 = reshape(csi_2,1,[]);
     
     
     I(index,:) = abs(csi_1);
     P(index,:) = angle(csi_1);
     
     index=index+1;
    
     current_ts = cell.timestamp_low;
     
     if(i<length(loged_data))
         
         next_ts = loged_data{i+1}.timestamp_low;
         diff_ts = next_ts - current_ts;
     else
         diff_ts = 300000;
     end
     
       
     if(diff_ts > 200000)
         
         %%denoise and save matrixes as mat files and reset I and P
         
         %firstly cut the 0 rows that was not fullfilled
         I=I(any(I,2),:);
         P=P(any(P,2),:);
         
         index=1;
               
         [DI,DP]= WaveDenoise(I,P,'sym6',6);
         
            DI = single(DI);
            DP = single(DP);
            
           
         % build the final matrix
         
          %we transpose the matrices because the sequenceInputLayer in the 
          %LSTM likes them with the time as Y axis
        
          final_trial = cat(1, transpose(DI),transpose(DP));
          outFile=strcat(outPath,name,int2str(trial_index),'.mat');
          save(outFile,'final_trial');         
          trial_index = trial_index+1;
         
         
         
          I=zeros(600,270);
          P=zeros(600,270);
        
         
     end
     
         
     
 end



end

