

function  plot_scaled_rssi(loged_data, start,stop,subcarriers)
% This function plots teh CSI Intesity using get_scaled_csi form linux-tool for a number of packet  based on start and stop
%   loged_data= #paresed packets form CSI
%   start = #packed index form witch the plot will start
%   stop =  #last packet that will be ploted
%   subcarriers = #maximum number of subcarriers
%       
%    Be careful to give the start and stop in the scope of loged date
%    matrix

 MC = zeros(stop-start,subcarriers);

for i=start:stop

cell = loged_data{i};

csi_3 = get_scaled_csi(cell);

csi_2 = reshape(csi_3,[],30);
csi_1 = reshape(csi_2,1,[]);

if(length(csi_1) < subcarriers)
    csi_1(subcarriers)=0;
end

MC((i+1)-start,:) = csi_1;
    
end
MR=abs(MC);
MI=imag(MC);
Mt = transpose(MR);

%3D plot
figure(2)
surf(Mt);
colormap(pink)    % change color map
shading interp 
xlabel('Packets')
ylabel('Subcarriers')
zlabel('RSSI')  % interpolate colors across lines and faces
grid
% plot for only 30 subcarriers
figure(4)
plot(Mt(1:30,:))
grid
%figure(5)

end

