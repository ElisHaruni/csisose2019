%% =====================================================================================
%% Trains an LSTM using CSI data.
%%
%%      Author:  Elis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================

function trainedNet = activityDetectionLSTM(trainSequences, trainLabels, testSequences, testLabels,miniBatchSize)

    % training parameters
    maxEpochs = 10;
    % BEWARE: to avoid losing some data in the last mini-batch of each epoch, make sure that the mini-batch size evenly divides the training set
    
    % network parameters
    numHiddenUnits = 100;
    numClasses = 10; % standing, walking, running, sitting, laying, falling, no activity
    classNames = categorical({'empty'; 'sitting2peop'; 'sitting3peop';'sitting1People'; 'sitting'; 'standing2peop';'standing';'standing1People';'walking2peop'; 'walking'});
 
    
    % set-up the network architecture
    layers = [ ...
        sequenceInputLayer(270) % subcarriers x (intensity, phase)
        lstmLayer(numHiddenUnits,'OutputMode','last')
        dropoutLayer(0.5)
        fullyConnectedLayer(numClasses)
        softmaxLayer('Name', 'finalSM') % to obtain a probability density function
        classificationLayer('Name', 'opinion','Classes', classNames)]; % compare the probability distribution to the one defined by the label
    
    % define the training options
    % 'Shuffle', 'every-epoch', ...
    options = trainingOptions('adam', ...
        'ExecutionEnvironment', 'cpu', ...
        'MaxEpochs', maxEpochs, ... % number of runs over the complete data-set
        'MiniBatchSize', miniBatchSize, ... % number of samples between weights update
        'ValidationData', {testSequences, testLabels}, ...
        'ValidationFrequency', 20, ...
        'Shuffle','every-epoch', ...
        'GradientThreshold', 1, ...
        'Verbose', false, ...
        'Plots', 'training-progress'); % show training progress

    % start training
    trainedNet = trainNetwork(trainSequences, trainLabels, layers, options);
    
end