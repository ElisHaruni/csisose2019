%% =====================================================================================
%% Creates a data set using all the files found in the provided path. Each file is
%% expected to contain the data from one trial. The resulting data-set is composed of an
%% array of time-sequences and of an array of labels.
%%
%%      Author:  Raul Bertone,Elis Haruni
%%      Email :  bertone@stud.fra-uas.de, haruni@stud.fra-uas.de
%%      
%% =====================================================================================

function [sequences, catLabels] = buildDataSet(path)

    % find all *.mat files at the provided path
    s = strcat(path, '*.mat');
    files = dir(s);
    n = length(files);
    sequences = cell(n, 1); % initialize the data array
    labels = strings(n, 1); % initialize the labels array

    % iterate over the trial files
    for i = 1:n    
        fName = files(i).name; % the name of the data file

        % add this trial to the data array
        s = strcat(path, fName);
        loaded = load(s);
        sequences{i, 1} = loaded.final_trial;
        
        % add label to the labels array. The label is derived from the
        % first part of the file name 
        labels(i, 1) = strtok(fName, '_');
    end

    catLabels = categorical(labels); % transform the labels array into a categorical one

end