

%% =====================================================================================
%% Denoise data Over Time using a special wavelet and level
%%
%%
%%      Author:  Elis Haruni
%% =====================================================================================


function [DI,DP] = WaveDenoise(I,P,name,level)
 DI = wdenoise(I,level,'Wavelet',name);
 DP = wdenoise(P,level,'Wavelet',name);

end

