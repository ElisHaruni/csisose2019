


%% =====================================================================================
%% Script to split the data 80/20 for training 
%% 
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


data_path= '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_phase/';
split_80_20(data_path);