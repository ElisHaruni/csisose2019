

%% =====================================================================================
%% function to help split of the data 80/20 for training 
%%  there might be issues with the paths since we use static paths.
%%
%%      Author:  ELis Haruni
%%      Email :  haruni@stud.fra-uas.de
%% =====================================================================================


function  split_80_20(postProcessedPath)
%SPLIT_80_20 Summary of this function goes here
%   Detailed explanation goes here
 
    test_data_path= '/Users/elisharuni/Documents/Learning/Schafer-Proj/csi_data_post_processing_onr_test/';
    s = strcat(postProcessedPath, '*.mat');
    files = dir(s);
    n = length(files);
    
   
   
    
    for i=5:n
        mod(i,5)
     if(mod(i,5)==0)
         test_sample= files(i).name;
          s = strcat(postProcessedPath, test_sample);
          loaded = load(s);
          final_trial = loaded.final_trial;
          filename= strcat(test_data_path,test_sample);
          save(filename,'final_trial');
          
         delete(s);
          
     end
    
    end 
    
  
  
    
end

